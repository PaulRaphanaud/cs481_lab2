import 'package:app/Widget/bottom_sheet_widget.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

class ActorsPage extends StatefulWidget {
  @override
  _ActorsPageState createState() => _ActorsPageState();
}

class _ActorsPageState extends State<ActorsPage> {
  Future _futureActor;

  Future _loadActor() async {
    String data =
        await DefaultAssetBundle.of(context).loadString('./assets/actors.json');
    return jsonDecode(data);
  }

  @override
  void initState() {
    _futureActor = _loadActor();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _futureActor,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              return ListTile(
                  title: Text("${snapshot.data[index]['name']}"),
                  subtitle: Text("${snapshot.data[index]['alternative_name']}"),
                  trailing: Column(
                    children: [
                      Icon(Icons.star_rate),
                      Text("${snapshot.data[index]['rating']}"),
                    ],
                  ),
                  leading: Icon(Icons.person),
                  onTap: () {
                    showModalBottomSheet(
                        context: context,
                        builder: (context) {
                          if (snapshot.data[index]['name'] != null) {
                            print(snapshot.data[index]['name']);
                            return BottomSheetInformation(
                              title: snapshot.data[index]['name'],
                            );
                          } else {
                            return Container();
                          }
                        });
                  });
            },
          );
        } else
          return Center(child: CircularProgressIndicator());
      },
    );
  }
}
