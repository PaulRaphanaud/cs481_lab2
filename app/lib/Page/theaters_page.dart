import 'package:app/Widget/bottom_sheet_widget.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

class Theaters extends StatefulWidget {
  @override
  _TheatersPageState createState() => _TheatersPageState();
}

class _TheatersPageState extends State<Theaters> {
  Future _futureTheaters;

  Future _loadTheaters() async {
    String data = await DefaultAssetBundle.of(context)
        .loadString('./assets/theaters.json');
    return jsonDecode(data);
  }

  @override
  void initState() {
    _futureTheaters = _loadTheaters();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _futureTheaters,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              return ListTile(
                  title: Text("${snapshot.data[index]['name']}"),
                  subtitle: Text("${snapshot.data[index]['address']}"),
                  trailing: Column(
                    children: [
                      Icon(Icons.star),
                      Text("${snapshot.data[index]['rating']}"),
                    ],
                  ),
                  leading: CircleAvatar(
                      backgroundImage:
                          AssetImage(snapshot.data[index]['image_path'])),
                  onTap: () {
                    showModalBottomSheet(
                        context: context,
                        builder: (context) {
                          if (snapshot.data[index]['name'] != null) {
                            print(snapshot.data[index]['name']);
                            return BottomSheetInformation(
                              title: snapshot.data[index]['name'],
                            );
                          } else {
                            return Container();
                          }
                        });
                  });
            },
          );
        } else
          return Center(child: CircularProgressIndicator());
      },
    );
  }
}
