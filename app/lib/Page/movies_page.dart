import 'dart:convert';

import 'package:app/Widget/bottom_sheet_widget.dart';
import 'package:flutter/material.dart';

class MoviesPage extends StatefulWidget {
  @override
  _MoviesPageState createState() => _MoviesPageState();
}

class _MoviesPageState extends State<MoviesPage> {
  @override
  void initState() {
    _loadMovies();
    super.initState();
  }

  Future _loadMovies() async {
    String data = await DefaultAssetBundle.of(context)
        .loadString('./assets/movies-in-theaters.json');
    return jsonDecode(data);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _loadMovies(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              String tmpGenres = '';
              for (var genre in snapshot.data[index]['genres'])
                if (tmpGenres != '')
                  tmpGenres += ' | ' + genre;
                else
                  tmpGenres += genre;

              return ListTile(
                title: Text(
                    "${snapshot.data[index]['title']} - ${snapshot.data[index]['year']}"),
                leading: Icon(Icons.movie),
                subtitle: Text(
                  "$tmpGenres",
                  style: TextStyle(fontSize: 12),
                ),
                trailing: SizedBox(
                  width: 50,
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 4.0),
                        child: Icon(Icons.star,
                            color: Colors.yellow[700], size: 22),
                      ),
                      Text(snapshot.data[index]['imdbRating'] == ""
                          ? "null"
                          : snapshot.data[index]['imdbRating'].toString())
                    ],
                  ),
                ),
                onTap: () {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        if (snapshot.data[index]['title'] != null) {
                          print(snapshot.data[index]['title']);
                          return BottomSheetInformation(
                            title: snapshot.data[index]['title'],
                            description: snapshot.data[index]['storyline'],
                          );
                        } else {
                          return Container();
                        }
                      });
                },
              );
            },
          );
        } else
          return Center(child: CircularProgressIndicator());
      },
    );
  }
}
