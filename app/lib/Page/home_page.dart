import 'package:flutter/material.dart';
import 'actors_page.dart';
import 'movies_page.dart';
import 'theaters_page.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedPage = 0;
  List<Widget> pageList = List<Widget>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  initState() {
    pageList.add(MoviesPage());
    pageList.add(ActorsPage());
    pageList.add(Theaters());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _onTapItem(int index) {
    setState(() {
      _selectedPage = index;
    });
  }

  void _bottomSheetCallback() {
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey, width: 2),
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: Text(
            'This App contain a list of different movies and actors.'
            'Use Bottom navigation bar to change pages',
            textAlign: TextAlign.left,
            style: TextStyle(fontSize: 22),
          ),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
              icon: const Icon(Icons.info), onPressed: _bottomSheetCallback)
        ],
      ),
      body: IndexedStack(
        index: _selectedPage,
        children: pageList,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedPage,
        selectedItemColor: Colors.blue,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            label: 'Movie',
            icon: Icon(Icons.movie, size: 30),
          ),
          BottomNavigationBarItem(
            label: 'Actors',
            icon: Icon(Icons.person, size: 30),
          ),
          BottomNavigationBarItem(
            label: 'Theaters',
            icon: Icon(Icons.business, size: 30),
          ),
        ],
        onTap: _onTapItem,
      ),
    );
  }
}
