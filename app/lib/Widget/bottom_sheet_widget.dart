import 'package:flutter/material.dart';

class BottomSheetInformation extends StatelessWidget {
  BottomSheetInformation({Key key, this.title, this.description})
      : super(key: key);

  @required
  final String title;
  final String description;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            title,
            style: TextStyle(fontSize: 25),
          ),
          SizedBox(
            height: 10,
          ),
          if (description != null)
            Text(
              description,
              style: TextStyle(fontSize: 15),
            ),
        ],
      ),
    );
  }
}
